from ox_api_client import OX_API_Client
from confluence import Confluence
import json

class CreateQuery():


	def __init__(self):
		return

	def start(self):
		data = self.get_api_data()
		self.get_api_data(data)
		self.write_api_data_to_wiki(data)


	def get_api_data(self, call=None):
		#call = '/lineitem/available_fields?pretty=1'
		call = '/adunit/available_fields?type_full=adunit.web'
		#call = '/order/available_fields'
		self.ox_api = OX_API_Client()
		self.ox_api.ox_api_session()
		self.ox_api.ox_api_login('openx-api@openx.org','Dogbert08',instance_name='instance16')
		api_data = self.ox_api.ox_api_get(call)
		return api_data

	def write_api_data(self, data, location=None):
		location = 'Users/david.eynon/Desktop/test'
		b = open(location, 'w')
		b.write(data)
		b.close()
		self.write_api_data_to_wiki(data)

	def write_api_data_to_wiki(self, data):
		from confluence import Confluence
		conf = Confluence(profile='confluence')
		print data
		pre = '{code}'
		data = pre + str(json.dumps(data,sort_keys=True, indent=1, separators=(',', ': ')))
		data = data +  '{code}'
		conf.storePageContent(page="TestDavidEynon",space="~david.eynon",content=data)



if __name__ == "__main__":
	print 'start'
	CreateQuery().start()
	print 'end'


'''
OX_API_Create_Object
OX_API_Create_Object           ${Object_Type}   ${ad_create_input}

'''