#!/usr/bin/python
import os

from   robot.api import logger
from   robot.libraries.BuiltIn import BuiltIn

FILE_PATH_LIST = {}

def ox_utils_file(file_path, repo="core"):
    """
    Given a short file name relative to oxbot-core/,
    return its full path name.
    """

    # file dir in core | e2e | api-v2 :
    if 'core' in repo.lower():
        result = '/Users/david.eynon/Projects/oxbot/api_doc' + '/' + file_path
    else:
        result = '/Users/david.eynon/Projects/oxbot/api_doc' + '/' + file_path

    return result

def ox_utils_new_file_with_counter(out_file_path):
    """
    Given a full path name of an output file, make it unique by adding
    a counter to the file name, each time this function is called.
    For example, the first time this function is called with the argument
    "/opt/ox/oxbot/oxresults/outfile.html", it will return
    "/opt/ox/oxbot/oxresults/outfile_1.html". The next time it will return
    "/opt/ox/oxbot/oxresults/outfile_2.html". Etc...
    """
    (out_file_base, out_file_ext) = os.path.splitext(out_file_path)
    if out_file_base in FILE_PATH_LIST:
       FILE_PATH_LIST[out_file_base] = FILE_PATH_LIST[out_file_base] + 1
    else:
       FILE_PATH_LIST[out_file_base] = 1

    new_base = out_file_base + '_' + FILE_PATH_LIST[out_file_base].__str__()
    return new_base + out_file_ext

def ox_utils_create_output_file(name, type, output_sub_dir, counter=True):
    """
    Create a full path name for an output file under the standard output
    directory. If output_sub_dir is given, add it to the standard output
    directory. Also include subdirectory levels with test suite name,
    test case name for uniqueness among different testcases during the
    same run. All intermediate subdirectories will be created.

    If counter is given, add a counter to the filename for uniqueness
    within the testcase. See ox_utils_new_file_with_counter().

    For example, a screenshot image should be stored under the screenshots/
    subdirectory under the standard output directory. The call
    ox_utils_create_output_file("image", "png", "screenshots", True) will
    return the full path name for
    OUTDIR/screenshots/<testsuitename>/<testcasename>/image_1.png
    """
    output_dir = BuiltIn().get_variable_value("${OUTPUT_DIR}")
    suite_name = BuiltIn().get_variable_value("${SUITE_NAME}")
    suite_name = suite_name.replace(" ", "_")
    test_name = BuiltIn().get_variable_value("${TEST_NAME}")
    test_name = test_name.replace(" ", "_")

    path = os.path.join(output_dir, output_sub_dir, suite_name, test_name)
    filename = name
    if type:
        filename = name + '.' + type
    full_filename = os.path.join(path, filename)

    output_file = full_filename
    if counter:
        output_file = ox_utils_new_file_with_counter(full_filename)

    logger.debug("Output_file %s" % output_file)
    basepath = os.path.dirname(output_file)
    if not os.path.exists(basepath):
        os.makedirs(basepath)
    return output_file

def ox_utils_write_output_file(content, name, type,
                               output_sub_dir="", counter=True):
    """
    Write content into an output file under the standard output directory.
    Ensure uniqueness of the output file by adding test suite name,
    and test case name in the subdirectories. Also, add a file counter
    if needed. See ox_utils_create_output_file() for more details.
    """
    full_filename = ox_utils_create_output_file(name,
                                                type, output_sub_dir, counter)
    f = open(full_filename, 'wb')
    try:
        f.write(content)
    except err:
        logger.debug("Cannot write to file %s" % full_filename)
        logger.debug("Error %s" % err)
    finally:
        f.close()
    return full_filename

def ox_utils_prefix_suffix_name(value, object_prefix=None,
                                user_prefix=None, suffix=None):
    """
    Prepend the value with object_prefix and user_prefix.
    No delimiter is added. The prefixes should contain any delimiter needed.
    Object_prefix will be prepended first, then user_prefix.
    Also append the value of suffix.
    New value will be UserprefixObjectprefixValueSuffix
    """
    if object_prefix:
        value = object_prefix + value
    if user_prefix:
        value = user_prefix + value
    if suffix:
        value = value + suffix
    return value
