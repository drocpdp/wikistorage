#-------------------------------------------------------------------------------
# oxbot-core/libraries/ox_json.py
#--------1---------2---------3---------4---------5---------6---------7---------8

# List of functions in this file: (alphabetically ordered)

#  ox_json_data_log(json_data, level="INFO"):
#  ox_json_data_filtered_should_be_equal(json_data1, json_data2, list_of_keys_to_compare=[],
#                    list_of_keys_to_ignore=[],list_of_keys_with_varying_vals=[], dict_with_known_keys_values1={},
#                    dict_with_known_keys_values2={}, msg=None, sort_array=False):
#  ox_json_data_sortfiltered_should_be_equal(json_data1,json_data2,list_of_keys_to_compare=[],
#                    list_of_keys_to_ignore=[], list_of_keys_with_varying_vals=[],
#                    dict_with_known_keys_values1={},dict_with_known_keys_values2={}, msg=None):
#  ox_json_dump(data):
#  ox_json_from_file(json_path):
#  ox_json_from_string(json_string):
#  ox_json_log(json_string, level="INFO"):
#  _ox_json_logger(json_data, is_on_first_level_dict=True,is_on_first_level_list=True):
#  ox_json_sort_list_of_dicts(data, sort_by):
#  ox_json_standard_placeholder(json_string):
#  ox_json_strings_filtered_should_be_equal(json_string1,json_string2,list_of_keys_to_compare=[],
#                    list_of_keys_to_ignore=[],list_of_keys_with_varying_vals=[],dict_with_known_keys_values1={},
#                    dict_with_known_keys_values2={}, msg=None, sort_array=False):
#  ox_json_strings_list_should_include(json_string1,json_string2,msg=None):
#  ox_json_strings_sortfiltered_should_be_equal(json_string1,json_string2,list_of_keys_to_compare=[],
#                    list_of_keys_to_ignore=[],list_of_keys_with_varying_vals=[],dict_with_known_keys_values1={},
#                    dict_with_known_keys_values2={},msg=None):
#  ox_json_filter(json_data,list_of_keys_to_compare=[],list_of_keys_to_ignore=[],
#                    list_of_keys_with_varying_vals=[], dict_of_known_keys_values={}, sort_array=False):
#  ox_test_of_json_filter():

#--------1---------2---------3---------4---------5---------6---------7---------8

from   robot.output import LOGGER, Message
from   robot.utils import asserts
import json
import oxbot_utils


#-------------------------------------------------------------------------------
def ox_json_data_log(json_data, level="INFO"):
#-------------------------------------------------------------------------------
    """
    Given a data structure (list or dictionary), convert it into a JSON string
    and log it using ox_json_log (ref.)
    """
    json_log = _ox_json_logger(json_data, True, True)
    LOGGER.log_message(Message(json_log, level))

#-------------------------------------------------------------------------------
def ox_json_data_filtered_should_be_equal(json_data1,
                                          json_data2,
                                          list_of_keys_to_compare=[],
                                          list_of_keys_to_ignore=[],
                                          list_of_keys_with_varying_vals=[],
                                          dict_with_known_keys_values1={},
                                          dict_with_known_keys_values2={},
                                          filters_2nd={},
                                          msg=None,
                                          sort_array=False):

#-------------------------------------------------------------------------------
    """
    Given 2 data structures
    1. filter them by
      a. either gather only keys to be compared
      b. or eliminate keys to be ignored
      c. replace entries where keys are known to have random value with VARYING
      d. replace entries where keys have known values with KNOWN - so that keys
         with different known values can be compared
    2. compare them as list or dict
    """

    json_data1_filtered = ox_json_filter(json_data1,
                                         list_of_keys_to_compare,
                                         list_of_keys_to_ignore,
                                         list_of_keys_with_varying_vals,
                                         dict_with_known_keys_values1,
                                         filters_2nd,
                                         sort_array)

    json_data2_filtered = ox_json_filter(json_data2,
                                         list_of_keys_to_compare,
                                         list_of_keys_to_ignore,
                                         list_of_keys_with_varying_vals,
                                         dict_with_known_keys_values2,
                                         filters_2nd,
                                         sort_array)

    json_log = _ox_json_logger(json_data1_filtered, True, True)
    LOGGER.log_message(Message(json_log, "DEBUG"))
    json_log = _ox_json_logger(json_data2_filtered, True, True)
    LOGGER.log_message(Message(json_log, "DEBUG"))

    LOGGER.log_message(Message("Dict1", "DEBUG"))
    json_log = _ox_json_logger(dict_with_known_keys_values1, True, True)
    LOGGER.log_message(Message(json_log, "DEBUG"))
    LOGGER.log_message(Message("Dict2", "DEBUG"))
    json_log = _ox_json_logger(dict_with_known_keys_values2, True, True)
    LOGGER.log_message(Message(json_log, "DEBUG"))


    # to help with debug, if json1 != json2:
    if (json_data1_filtered != json_data2_filtered):
        print "Adding logs if: json1 != json2"
        json1 = json_data1_filtered
        json2 = json_data2_filtered
        if (json1 and isinstance(json1, list) and isinstance(json1[0], dict)):
            # if a list only has 1 item, and this item is dict:
            json1 = json1[0]
        if (json2 and isinstance(json2, list) and isinstance(json2[0], dict)):
            json2 = json2[0]
        elif not json1:
            LOGGER.log_message(Message("json1 list is empty", "DEBUG"))
            json1 = None
        elif (json2 == []):
            LOGGER.log_message(Message("json2 list is empty", "DEBUG"))
            json2 = None

        if isinstance(json1, dict) and isinstance(json2, dict):
            key1_list = json1.keys()
            for key1 in key1_list:
                if (key1 in json2) and json1[key1]==json2[key1]:
                    del json1[key1]
                    del json2[key1]
            if json1:
                print json.dumps(json1, sort_keys=True, indent=4)
            else:
                LOGGER.log_message(Message("json1 dict is empty", "DEBUG"))
            if json2:
                print json.dumps(json2, sort_keys=True, indent=4)
            else:
                LOGGER.log_message(Message("json2 dict is empty", "DEBUG"))

        if isinstance(json1, list) and isinstance(json2, list):
            json1_copy = list(json1)
            json2_copy = list(json2)
            for item in json1_copy:
                if item in json2_copy:
                    json1.remove(item)
                    json2.remove(item)
            print 'Remaining part of json1: %s' % json1
            print 'Remaining part of json2: %s' % json2

    # to help with debug, if json1 != json2:
    if (json_data1_filtered != json_data2_filtered):
        print "Adding logs again if: json1 != json2"
        import pprint
        import difflib
        diff = ('\n' + '\n'.join(difflib.ndiff(
            pprint.pformat(json_data1_filtered).splitlines(),
            pprint.pformat(json_data2_filtered).splitlines())))
        diff = [l for l in diff.split('\n') if (l.startswith('+') or l.startswith('-'))]
        diff = "\n".join(diff)
        print "diff= "
        print diff

    asserts.fail_unless_equal(json_data1_filtered,
                              json_data2_filtered,
                              msg,
                              False)

#-------------------------------------------------------------------------------
def ox_json_data_sortfiltered_should_be_equal(json_data1,
                                              json_data2,
                                              list_of_keys_to_compare=[],
                                              list_of_keys_to_ignore=[],
                                              list_of_keys_with_varying_vals=[],
                                              dict_with_known_keys_values1={},
                                              dict_with_known_keys_values2={},
                                              filters_2nd={},
                                              msg=None):

#-------------------------------------------------------------------------------
    """
    Given 2 data structures
    1. compare them by using the method ox_json_data_filtered_should_be_equal
       The arrays (if any) in the data structures will be sorted before
       comparison, with sort_array=True
    """
    ox_json_data_filtered_should_be_equal(json_data1,
                                          json_data2,
                                          list_of_keys_to_compare,
                                          list_of_keys_to_ignore,
                                          list_of_keys_with_varying_vals,
                                          dict_with_known_keys_values1,
                                          dict_with_known_keys_values2,
                                          filters_2nd,
                                          msg,
                                          sort_array=True)

#-------------------------------------------------------------------------------
def ox_json_dump(data):
#-------------------------------------------------------------------------------
    """
    Given a data structure (list or dictionary), convert it into a JSON string
    Returns the JSON string
    """
    json_string = json.dumps(data)
    return json_string

#-------------------------------------------------------------------------------
def ox_json_from_file(json_path, repo="core"):
#-------------------------------------------------------------------------------
    """
    Given a file path as argument, open the file then parse and return JSON.
    File path should be specified relative to OXBOT_HOME setting.
    Return appropriate RF list type. See Collections Library docs.
    """
    file_path = oxbot_utils.ox_utils_file(json_path, repo)
    filej=open(file_path)
    parsed = json.loads(filej.read())
    return parsed

#-------------------------------------------------------------------------------
def ox_json_from_string(json_string):
#-------------------------------------------------------------------------------
    """
    Given a string containing JSON as argument, parse and return JSON.
    Return appropriate RF list type. See Collections Library docs.
    """
    parsed = json.loads(json_string)
    return parsed

#-------------------------------------------------------------------------------
def ox_json_log(json_string, level="INFO"):
#-------------------------------------------------------------------------------
    """
    Given a JSON string, log the string in the debug.log file in a nice
    alignment. Dictionaries will be sorted by their keys.  Key/value pairs
    from the first level will be written on separate lines. Key/value pairs
    for levels below that will be written on the same line.
    """
    json_data = json.loads(json_string)
    ox_json_data_log(json_data, level)

#-------------------------------------------------------------------------------
def _ox_json_logger(json_data,
                    is_on_first_level_dict=True,
                    is_on_first_level_list=True):
#-------------------------------------------------------------------------------
    """
    Print the JSON structure in a pretty format. Dictionaries will be sorted
    by keys. The output can be cut-n-paste into data files.
    """
    json_log = ""
    if isinstance(json_data, list):
        current_is_on_first_level_list = is_on_first_level_list
        if is_on_first_level_list:
            is_on_first_level_list = False
        continuation = ""
        json_log += "[ "
        for index, value in enumerate(json_data):
            json_log += continuation
            if current_is_on_first_level_list:
                json_log += "\n "
            json_log += _ox_json_logger(value,
                                        is_on_first_level_dict,
                                        is_on_first_level_list)
            continuation = ", "
        if current_is_on_first_level_list:
            json_log += "\n  ]\n"
        else:
            json_log += " ]"
    elif isinstance(json_data, dict):
        current_is_on_first_level_dict = is_on_first_level_dict
        if is_on_first_level_dict:
            is_on_first_level_dict = False
        continuation = ""
        json_log += "{ "
        for key in sorted(json_data.iterkeys()):
            json_log += continuation
            if current_is_on_first_level_dict:
                json_log += "\n "
            json_log += '"' + key + '": '
            json_log += _ox_json_logger(json_data[key],
                                        is_on_first_level_dict,
                                        is_on_first_level_list)
            continuation = ", "
        if current_is_on_first_level_dict:
            json_log += "\n  }\n"
        else:
            json_log += " }"
    elif isinstance(json_data, bool):
        if json_data:
            json_log += "true"
        else:
            json_log += "false"
    elif isinstance(json_data, int) or isinstance(json_data, float):
        json_log += json_data.__str__()
    elif json_data is None:
        json_log += "null"
    elif isinstance(json_data, unicode):
        json_log += '"' + unicode(json_data) + '"'
    else:
        json_log += '"' + json_data.__str__() + '"'
    return json_log

#-------------------------------------------------------------------------------
def ox_json_sort_list_of_dicts(data, sort_by):
#-------------------------------------------------------------------------------
    """
    Given a list containing dicts, sort the list based on the
    values of the key "sort_by"
    Returns a sorted list
    """
    if not isinstance(data, list):
        return data

    sorted_data = []
    for item in data:
        position = 0
        for index, sitem in enumerate(sorted_data):
            value = item[sort_by]
            svalue = sitem[sort_by]
            if isinstance(value, basestring) and value.isdigit():
                value = int(value)
            if isinstance(svalue, basestring) and svalue.isdigit():
                svalue = int(svalue)
            if value >= svalue:
                position = index + 1
        sorted_data.insert(position, item)
    return sorted_data

#-------------------------------------------------------------------------------
def ox_json_standard_placeholder(json_string):
#-------------------------------------------------------------------------------
    """
    Given a JSON string representing a dictionary (or list of dictionaries),
    1. in the case of a dictionary list, use only the first item
    2. extract all the entries that match the pattern "key": "placeholder_key"
    3. returns a dictionary containing entries described above
    """
    standard_placeholders = {}
    json_data = json.loads(json_string)
    if isinstance(json_data, list):
        json_data = json_data[0]
    if isinstance(json_data, dict):
        for key in sorted(json_data.iterkeys()):
            if json_data[key] == "placeholder_" + key:
                standard_placeholders[key] = json_data[key]
    return standard_placeholders

#-------------------------------------------------------------------------------
def ox_json_strings_filtered_should_be_equal(json_string1,
                                             json_string2,
                                             list_of_keys_to_compare=[],
                                             list_of_keys_to_ignore=[],
                                             list_of_keys_with_varying_vals=[],
                                             dict_with_known_keys_values1={},
                                             dict_with_known_keys_values2={},
                                             filters_2nd={},
                                             msg=None,
                                             sort_array=False):

#-------------------------------------------------------------------------------
    """
    Given 2 JSON strings,
    1. convert them into data structure - json_data
    2. compare them by using the method ox_json_data_filtered_should_be_equal
    """

    json_data1 = json.loads(json_string1)
    json_data2 = json.loads(json_string2)
    ox_json_data_filtered_should_be_equal(json_data1,
                                          json_data2,
                                          list_of_keys_to_compare,
                                          list_of_keys_to_ignore,
                                          list_of_keys_with_varying_vals,
                                          dict_with_known_keys_values1,
                                          dict_with_known_keys_values2,
                                          filters_2nd,
                                          msg,
                                          sort_array)


#-------------------------------------------------------------------------------
def ox_json_strings_list_should_include(json_string1,
                                        json_string2,
                                        msg=None):
#-------------------------------------------------------------------------------
    """
    """
    large_data = json.loads(json_string1)
    small_data = json.loads(json_string2)

    json_log = _ox_json_logger(small_data, True, True)
    LOGGER.log_message(Message("Data to match", "DEBUG"))
    LOGGER.log_message(Message(json_log, "DEBUG"))

    matched = False
    for index, value in enumerate(large_data):
        json_log = _ox_json_logger(value, True, True)
        LOGGER.log_message(Message("Trying ... ", "DEBUG"))
        LOGGER.log_message(Message(json_log, "DEBUG"))
        if small_data == value:
            matched = True
            break

    asserts.fail_unless_equal(matched,
                              True,
                              msg,
                              False)

#-------------------------------------------------------------------------------
def ox_json_strings_sortfiltered_should_be_equal(json_string1,
                                             json_string2,
                                             list_of_keys_to_compare=[],
                                             list_of_keys_to_ignore=[],
                                             list_of_keys_with_varying_vals=[],
                                             dict_with_known_keys_values1={},
                                             dict_with_known_keys_values2={},
                                             filters_2nd={},
                                             msg=None):

#-------------------------------------------------------------------------------
    """
    Given 2 JSON strings,
    1. convert them into data structure - json_data
    2. compare them by using the method ox_json_data_filtered_should_be_equal
    """
    ox_json_strings_filtered_should_be_equal(json_string1,
                                             json_string2,
                                             list_of_keys_to_compare,
                                             list_of_keys_to_ignore,
                                             list_of_keys_with_varying_vals,
                                             dict_with_known_keys_values1,
                                             dict_with_known_keys_values2,
                                             filters_2nd,
                                             msg,
                                             sort_array=True)


#-------------------------------------------------------------------------------
def ox_json_filter(json_data,
                   list_of_keys_to_compare=[],
                   list_of_keys_to_ignore=[],
                   list_of_keys_with_varying_vals=[],
                   dict_of_known_keys_values={},
                   filters_2nd={},
                   sort_array=False):

#-------------------------------------------------------------------------------
    """
    === Bug (Emily reported 2013.8.9): ===
    If there is a dict2 inside dict1, and dict2 has same key as dict, and that key
    in dict1 is a known_value, then the value inside dict2 with the same key will
    be changed too. Need to fix it.

    To trigger this bug, use: oxtest-api/tier0/tier0_adproduct_basic_crud.txt,
    remore the 'components' from varying list in the variable part, then the
    compare in *** Keywords *** section will trigger the bug.
    === End of bug info ===

    Given a data structure, cleanse the data so that we can use it to compare
    against a baseline. Typically, these are data that vary between different
    test runs, such as created_data, modified_date, id, etc...
    1. We want to be able to pick out only certain entries to compare. We will
       keep these entries and ignore (delete) the rest.
    2. Alternatively, we want to pick out certain entries to ignore, i.e.
       the rest of the entries will be compared. We will ignore (delete)
       these entries and keep the rest. #1 above takes precedence, i.e. if
       a key exists in list_of_keys_to_compare and list_of_keys_to_ignore,
       the entry will be kept for comparison.
    Of the keys that are kept for comparison,
    3. We want to ignore values for entries with certain keys. The entries
       with these keys are still checked for existence, but the values will
       be stubbed out. This is useful when the values vary from test run to
       test run (e.g. created_date, id) AND we can't predict the values.
    4. We want to ignore values for entries with certain key/value pair.
       The entries with those key/value are still checked for existence, but
       the values will be stubbed out if they match expected values. This is
       useful if we have 2 different calls returning different known values
       for certain keys, and we still want to check their values. For example,
       we expect one call to return status='Active' and another to return
       status='Pending'. Another example is when we want to compare parent id
       of an object. The parent id may change from test run to test run, but
       within each test run, we should have the value we expect.
    5. Lastly, we want to have the option of leaving arrays as is (i.e. sort
       order does matter), or sort all arrays (i.e. sort order does not matter)
       for easier comparison later.
    Args: json_data - Data structure (not a JSON string)
          list_of_keys_to_ignore - Entries with these keys will be deleted.
          list_of_keys_to_compate - Entries with these keys will be kept.
              The rest of the entries will be deleted.
          list_of_keys_with_varying_vals - Entries with these keys will have
              their values stubbed out (i.e. replaced with 'XxVARYINGxX').
          dict_of_known_keys_values - Dictionary of keys with known (expected)
              values. Entries matching these key/value pairs will have their
              values stubbed out (i.e. replaced with 'XxKNOWNxX').
          filters_2nd is a the 2nd level filter. It is a dictionary. It looks
              like:  filters_2nd{
                                  "list_of_keys_to_compare":[a,b,c],
                                  "list_of_keys_to_ignore":[d,e,f,g],
                                  "list_of_keys_with_varying_vals":[i,j],
                                  "dict_of_known_keys_values":{"a":"b",...}
                                 }
          sort_array - True or False
    """

    if isinstance(json_data, list):
        if sort_array:
            json_data.sort()
        new_json_data = []
        for index, value in enumerate(json_data):
            new_data = ox_json_filter(value,
                                      list_of_keys_to_compare,
                                      list_of_keys_to_ignore,
                                      list_of_keys_with_varying_vals,
                                      dict_of_known_keys_values,
                                      filters_2nd,
                                      sort_array)
            new_json_data.append(new_data)
    elif isinstance(json_data, dict):
        new_json_data = json_data.copy()
        for key, value in json_data.items():
            # Decide if we need to ignore this entry
            if list_of_keys_to_compare:
                if key not in list_of_keys_to_compare:
                    del new_json_data[key]
                    continue
                elif list_of_keys_to_ignore:
                    if key in list_of_keys_to_ignore:
                        del new_json_data[key]
                        continue
            elif list_of_keys_to_ignore:
                if key in list_of_keys_to_ignore:
                    del new_json_data[key]
                    continue

            # Now that we are not ignoring this entry, decide if we
            # should ignore its value
            if list_of_keys_with_varying_vals:
                if key in list_of_keys_with_varying_vals:
                    new_json_data[key] = 'XxVARYINGxX'
                    continue
            if dict_of_known_keys_values:
                if key in dict_of_known_keys_values:
                    if json_data[key] == dict_of_known_keys_values[key]:
                        new_json_data[key] = 'XxKNOWNxX'
                        continue
                    else:
                        # Set up the data so that comparison will fail
                        rand_value = random.randrange(1,10000).__str__()
                        new_json_data[key + '_' + rand_value] = 'XxFAILxX'

            # EZ 9.23.2013: this was a bug. When we filter for the 2nd level, we can not use the filters (compare, ignore, ..)
            #    that we passed in for the 1st level. We need to have seperate filters for each level, I added below, called filters_2nd.
            #    Right now, API only has 2 levels at most, usually. Later we might want to make this work for any number of levels.

            # Now filter the value, because it may be a list or a dict
            if ((isinstance(new_json_data[key], dict)) and (filters_2nd != {})):
                filters_temp = filters_2nd
                new_json_data[key] = ox_json_filter(value,
                                                    filters_temp['list_of_keys_to_compare'],
                                                    filters_temp['list_of_keys_to_ignore'],
                                                    filters_temp['list_of_keys_with_varying_vals'],
                                                    filters_temp['dict_of_known_keys_values'],
                                                    {},
                                                    filters_temp['sort_array'])

    else:
        new_json_data = json_data

    return new_json_data


#-------------------------------------------------------------------------------
def ox_test_of_json_filter():
#-------------------------------------------------------------------------------
    a = [{'one': 'uno', 'two': 'dos', 'three': 'tres', 'four': 'quatro'},
         {'one': 'mot', 'two': 'hai', 'three': 'ba'}]
    list_one = ['one']
    list_three = ['three']
    list_four = ['four']
    list_one_two = ['one', 'two']
    list_four_three_one = ['four', 'three', 'one']
    dict_one_two = {'one': 'uno', 'two': 'dos'}
    dict_four = {'four': 'quatro'}

    # Test 1
    expected_b = [{'four': 'quatro', 'three': 'tres'}, {'three': 'ba'}]
    b = ox_json_filter(a, list_of_keys_to_ignore=list_one_two)
    if b != expected_b:
        print 'Failed test 1'
    else:
        print 'Pass test 1'

    # Test 2
    expected_b = [{'one': 'uno', 'two': 'dos'}, {'one': 'mot', 'two': 'hai'}]
    b = ox_json_filter(a, list_of_keys_to_compare=list_one_two)
    if b != expected_b:
        print 'Failed test 2'
    else:
        print 'Pass test 2'

    # Test 3
    expected_b = [{'four': 'quatro', 'one': 'XxVARYINGxX', 'three': 'tres',
                   'two': 'XxVARYINGxX'},
                  {'one': 'XxVARYINGxX', 'three': 'ba', 'two': 'XxVARYINGxX'}]
    b = ox_json_filter(a, list_of_keys_with_varying_vals=list_one_two)
    if b != expected_b:
        print 'Failed test 3'
    else:
        print 'Pass test 3'

    # Test 4
    expected_b = [{'four': 'quatro', 'one': 'XxKNOWNxX', 'three': 'tres',
                   'two': 'XxKNOWNxX'},
                  {'one': 'mot', 'three': 'ba', 'two': 'hai'}]
    b = ox_json_filter(a, dict_of_known_keys_values=dict_one_two)
    if b != expected_b:
        print 'Pass test 4'
    else:
        print 'Failed test 4'

    # Test 5
    expected_b = [{'four': 'XxKNOWNxX', 'one': 'uno', 'three': 'XxVARYINGxX',
                   'two': 'dos'},
                  {'one': 'mot', 'three': 'XxVARYINGxX', 'two': 'hai'}]
    b = ox_json_filter(a, dict_of_known_keys_values=dict_four,
                    list_of_keys_with_varying_vals=list_three)
    if b != expected_b:
        print 'Failed test 5'
    else:
        print 'Pass test 5'

    # Test 6
    expected_b = [{'four': 'XxKNOWNxX', 'three': 'XxVARYINGxX',
                   'two': 'dos'},
                  {'three': 'XxVARYINGxX', 'two': 'hai'}]
    b = ox_json_filter(a, dict_of_known_keys_values=dict_four,
                    list_of_keys_with_varying_vals=list_three,
                    list_of_keys_to_ignore=list_one)
    if b != expected_b:
        print _ox_json_logger(b)
        print 'Failed test 6'
    else:
        print 'Pass test 6'

    # Test 7
    expected_b = [{'four': 'quatro'}, {}]
    b = ox_json_filter(a, dict_of_known_keys_values=dict_one_two,
                    list_of_keys_with_varying_vals=list_three,
                    list_of_keys_to_compare=list_four)
    if b != expected_b:
        print 'Failed test 7'
    else:
        print 'Pass test 7'

    # Test 8
    expected_b = [{'four': 'quatro', 'three': 'XxVARYINGxX'},
                  {'three': 'XxVARYINGxX'}]
    b = ox_json_filter(a, dict_of_known_keys_values=dict_one_two,
                    list_of_keys_with_varying_vals=list_three,
                    list_of_keys_to_compare=list_four_three_one,
                    list_of_keys_to_ignore=list_one)
    if b != expected_b:
        print 'Failed test 8'
    else:
        print 'Pass test 8'

    # Test 9
    expected_b = a
    b = ox_json_filter(a)
    if b != expected_b:
        print 'Failed test 9'
    else:
        print 'Pass test 9'
#-------------------------------------------------------------------------------
