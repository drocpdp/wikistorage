from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.action_chains import ActionChains
import time


def verify_iframe_image(url,ids,href):
    """
    Looks up the image href attribute for an iframe
    """
    try:
        wd = WebDriver()
        wd.implicitly_wait(60)
        success = True
        wd.get(url)
        wd.switch_to_frame(int(ids))
        if wd.find_element_by_xpath("//a").get_attribute("href") != href:
            success = False
            print("verifyElementAttribute failed")
    finally:
        wd.quit()
        if not success:
            raise Exception("Test failed.")