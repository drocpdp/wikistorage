#!/usr/bin/env python

# =========================
#   This module does object creation:

#    1.  Name uniqueness:              adding runtime random number
#    2.  Default creation data:        user input is minimized, see below for "Minimum Input data needed".
#    3.  Combine default & input:      user can choose to use default data,
#                                           by combine default_data with input_data
#                                      user can also choose to use default=False,
#                                           and only use input_data
#    4.  Allow key/value pair input:   see Example sections below.
#    5.  Object type_full:             create for each type_full, ex: ox_create_li_network
#    6.  Assert success/failure:       by default assert successful, user can input expected_code

# =========================
#   Minimum input data needed, and full list of functions:
#
#    1,  ox_create_ad_html:                 lineitem_uid
#    2,  ox_create_ad_exch_html:            lineitem_uid
#    3,  ox_create_ad_exch_ssrtb:           lineitem_uid
#    4,  ox_create_adproduct:               account_uid
#    5,  ox_create_adunit_web:              site_uid
#    6,  ox_create_adunitgroup:             site_uid,  masteradunit_uid, adunit_uids
#    7,  ox_create_adv:                     account_uid
#    8,  ox_create_adv_exch:                account_uid
#    9,  ox_create_audiencesegment:         account_uid
#   10,  ox_create_conversiontag:           account_uid
#   11,  ox_create_creative:                account_uid
#   12,  ox_create_deal:                    account_uid, package_uid
#   13,  ox_create_li_exclusive:            order_uid
#   14,  ox_create_li_house:                order_uid
#   15,  ox_create_li_net:                  order_uid
#   16,  ox_create_li_ng:                   order_uid
#   17,  ox_create_li_preferred:            order_uid
#   18,  ox_create_li_pricebid:             order_uid
#   19,  ox_create_li_sov:                  order_uid
#   20,  ox_create_li_ssrtb:                order_uid
#   21,  ox_create_li_vg:                   order_uid
#   22,  ox_create_network:                 account_uid
#   23,  ox_create_optimization:            account_uid
#   24,  ox_crewate_order:                  account_uid
#   25,  ox_create_package:                 account_uid
#   26,  ox_create_publisher:               account_uid
#   27,  ox_create_publisher_self_serve:    account_uid
#   28,  ox_create_site:                    account_uid
#   29,  ox_create_sitesection:             site_uid
#   30,  ox_create_user_net_admin:          account_uid
#   31,  ox_create_user_net_report:         account_uid
#   32,  ox_create_user_pub_admin:          account_uid
#   33,  ox_create_user_pub_report:         account_uid
#
# =========================
#  Examples on how to call functions in this module:
#
#    1,  Simplest case: call the function only with parent uid.
#        This will create an object, expect success.
#            ox_create_ad_html         lineitem_uid=<uid>
#            ox_create_ad_exch_html    lineitem_uid=<uid>    expected_code=403     (403 mean forbidden)
#
#    2,  Wrap all input fields in Input_Data, must be of type dict:
#        If any input from input_data has overlap with default_data, default will be overwritten.
#            ox_create_li_ng        ${Input_Data}
#            ox_create_order        ${Input_Data}       expected_code=400
#
#    3,  Have both Input_Data and key/value pairs:
#        status=Active and name=BIG here are the key/value pairs, and they will be added to Input_Data automatically.
#        If there is already status/name in Input_Data, the existing status/name will be overwritten.
#            ox_create_li_ng        ${Input_Data}    status=Active    name=something    suffix=${SUFFIX}   expected_code=400
#
#    4,  Most complicated case:
#        Note below, although we suggest grouping all input fields together (move name up), the line below will still work.
#        The final Input_Data will include: status, account_uid, experience, name
#            ox_create_adv          status=Inactive      account_uid=<uid>   experience=advertier.exchange   suffix=123
#            ...                    user_prefix=qa0_api  object_prefix=I10   expected_code=403               name=someName
#
# =========================


from datetime import datetime, timedelta
from random import randint
from robot.api import logger
from robot.libraries.BuiltIn import BuiltIn
from ox_api_client import OX_API_Client as client


SUCCESSFUL_API_CALL_CODE = 200
url_session = '/session'


default_ad_html_data = {
    "name": "OXbot_Ad_Html",
    "size": "300x600",
    "status": "Active",
    "type_full": "ad.html"
}


default_ad_exch_html_data = {
    "ad_category": "4",
    "creative_types": {
        "1": True
    },
    "name": "OXbot_Ad_Exch_Html",
    "size": "300x600",
    "status": "Active",
    "type_full": "ad.exchange.html"
}

default_ad_exch_ssrtb_data = {
    "ad_category": "4",
    "creative_types": {
        "1": True
    },
    "name": "OXbot_Ad_Exch_SSRTB",
    "size": "300x600",
    "status": "Active",
    "type_full": "ad.exchange.ssrtb"
}

default_adproduct_data = {
    "components": [
        {
            "ad_delivery": "equal",
            "delivery_medium": "WEB",
            "name": "Li_Inside_Adproduct"
        }
    ],
    "floor_cpm": "99.0000",
    "name": "OXbot_AdProduct",
    "status": "Active"
}


default_adunit_web_data = {
    "name": "OXbot_Adunit_Web",
    "status": "Active",
    "tag_type": "js.sync",
    "type_full": "adunit.web"
}

default_adunitgroup_data = {
    "delivery_medium": "WEB",
    "name": "OXbot_Adunitgroup",
    "status": "Active"
}

default_adv_data = {
    "country_of_business": "us",
    "currency": "USD",
    "experience": "advertiser",
    "name": "OXbot_Adv",
    "status": "Active",
    "timezone": "UTC",
    "type_full": "account.advertiser"
}

default_adv_exch_data = {
    "acl_override": {
        "advertiser.exchange.priced_bid.read": True,
        "advertiser.exchange.priced_bid.write": True,
        "advertiser.exchange.ssrtb.read": True,
        "advertiser.exchange.ssrtb.write": True
    },
    "name": "OXbot_Adv_Exch",
    "experience": "advertiser.exchange",
    "exchange": {}
}

default_adv_exch_ssrtb_data = {
    "exchange": {
        "ssrtb": {
            "endpoints":[
                {
                    "name": "us_east",
                    "bid_url": "http://127.0.0.1:12361/?bidder=100&bid=0.02",
                    "notification_url": "http://127.0.0.1:12361/notify?bidder=100"
                },
                {
                    "name": "us_west",
                    "bid_url": "http://127.0.0.1:12361/?bidder=100&bid=0.02",
                    "notification_url": "http://127.0.0.1:12361/notify?bidder=100"
                },
                {
                    "name": "eu",
                    "bid_url": "http://127.0.0.1:12361/?bidder=100&bid=0.02",
                    "notification_url": "http://127.0.0.1:12361/notify?bidder=100"
                },
                {
                    "name": "japan",
                    "bid_url": "http://127.0.0.1:12361/?bidder=100&bid=0.02",
                    "notification_url": "http://127.0.0.1:12361/notify?bidder=100"
                }
            ],
            "protocol": "ox_proto"
        }
    }
}

default_audiencesegment_data = {
    "name": "OXbot_AudienceSegment",
    "status": "Active"
}

default_conversiontag_data = {
    "action_type": "lead",
    "name": "OXbot_Conversiontag",
    "status": "Active"
}

default_creative_data = {
    "ad_type_full": "ad.html",
    "name": "OXbot_Creative",
    "uri": "http://www.oxbot.org"
}

default_deal_data = {
    "deal_participants": [],
    "deal_price": "0.02",
    "deal_priority": "10",
    "name": "OXbot_Deal",
    "open_auction_access": "1",
    "pmp_deal_type": "1",
    "start_date": "2014-04-01 00:00:00",
    "status": "Active"
}

default_li_exclusive_data = {
    "delivery_medium": "WEB",
    "name": "OXbot_Li_Exclusive",
    "pricing_model": "cpm",
    "pricing_rate": "2.00",
    "status": "Pending",
    "type_full": "lineitem.exclusive"
}

default_li_house_data = {
    "delivery_medium": "WEB",
    "name": "OXbot_Li_House",
    "status": "Pending",
    "type_full": "lineitem.house"
}

default_li_net_data = {
    "delivery_medium": "WEB",
    "name": "OXbot_Li_Network",
    "pricing_rate": "0.02",
    "status": "Pending",
    "type_full": "lineitem.network"
}

default_li_ng_data = {
    "delivery_medium": "WEB",
    "name": "OXbot_Li_Non_Guaranteed",
    "pricing_model": "cpm",
    "pricing_rate": "2.0000",
    "status": "Pending",
    "type_full": "lineitem.non_guaranteed"
}

default_li_preferred_data = {
    "delivery_medium": "WEB",
    "name": "OXbot_Li_Preferred",
    "pricing_model": "cpm",
    "pricing_rate": "2.0000",
    "status": "Pending",
    "type_full": "lineitem.preferred"
}

default_li_pricedbid_data = {
    "bid_price":"9.0",
    "bid_type":"PRICEDBID",
    "budget":"15000.00",
    "budget_type":"DAILY",
    "budget_spend_rate":"QUICKLY",
    "delivery_medium": "WEB",
    "name": "OXbot_Li_PRICEDBID",
    "status": "Pending",
    "type_full": "lineitem.exchange"
}

default_li_shareofvoice_data = {
    "delivery_medium": "WEB",
    "name": "OXbot_Li_ShareOfVoice",
    "pricing_model": "cpm",
    "pricing_rate": "2.00",
    "share_of_voice": "12.3",
    "status": "Pending",
    "type_full": "lineitem.share_of_voice"
}

default_li_ssrtb_data = {
    "bid_price":"9.0",
    "bid_type": "SSRTB",
    "budget": "15000.00",
    "budget_type": "TOTAL",
    "daily_click_cap": "10",
    "delivery_medium":"WEB",
    "name" : "OXbot_Li_SSRTB",
    "status": "Pending",
    "targeting": {},
    "type_full":"lineitem.exchange"
}

default_li_volumegoal_data = {
    "delivery_medium": "WEB",
    "lifetime_impression_goal": "1000000",
    "name": "OXbot_Li_VG",
    "pricing_model": "cpm",
    "pricing_rate": "2.00",
    "priority": "1",
    "status": "Pending",
    "type_full": "lineitem.volume_goal"
}

default_network_data = {
    "country_of_business": "us",
    "currency": "USD",
    "experience": "network.display",
    "name": "OXbot_Network",
    "status": "Active",
    "timezone": "UTC",
    "type_full": "account.network"
}

default_optimization_data = {
    "delivery_medium": "WEB",
    "name": "OXbot_Optimization"
}

default_order_data = {
    "end_date": "2018-01-01 00:00:00",
    "name": "OXbot_Order",
    "start_date": "2014-01-01 00:00:00",
    "status": "Pending"
}

default_package_data = {
    "name": "OXbot_Package"
}

default_pub_data = {
    "country_of_business": "us",
    "currency": "USD",
    "experience": "publisher",
    "name": "OXbot_Enterprise_Publisher",
    "status": "Active",
    "timezone": "UTC",
    "type_full": "account.publisher"
}

default_pub_self_serve_data = {
    "country_of_business": "us",
    "currency": "USD",
    "experience": "publisher.self_serve",
    "name": "OXbot_Self-Serve_Publisher",
    "status": "Active",
    "timezone": "UTC",
    "type_full": "account.publisher"
}

default_site_data = {
    "name": "OXbot_Site",
    "status": "Active",
    "url": "http://www.emily.com"
}

default_sitesection_data = {
    "name": "OXbot_Sitesection",
    "status": "Active"
}

default_user_net_admin_data = {
    "first_name": "OXbot_firstname",
    "last_name": "OXbot_lastname",
    "email":  "OXbot_User_Network_Admin@openx.com",
    "role": "network.admin",
    "status": "Active"
}

default_user_net_report_data = {
    "first_name": "OXbot_firstname",
    "last_name": "OXbot_lastname",
    "email":  "OXbot_User_Network_Report@openx.com",
    "role": "network.reporting",
    "status": "Active"
}

default_user_pub_admin_data = {
    "first_name": "OXbot_firstname",
    "last_name": "OXbot_lastname",
    "email":  "OXbot_User_Publisher_Admin@openx.com",
    "role": "publisher.admin",
    "status": "Active"
}

default_user_pub_report_data = {
    "first_name": "OXbot_firstname",
    "last_name": "OXbot_lastname",
    "email":  "OXbot_User_Publisher_Report@openx.com",
    "role": "publisher.reporting",
    "status": "Active"
}

mapper = {'ox_create_ad_html': {'object_type':'ad','template':default_ad_html_data},
            'ox_create_ad_exch_html': {'object_type':'ad','template':default_ad_exch_html_data},
            'ox_create_ad_exch_ssrtb': {'object_type':'ad', 'template': default_ad_exch_ssrtb_data},
            'ox_create_adproduct': {'object_type':'adproduct', 'template': default_adproduct_data},
            'ox_create_adunit_web': {'object_type':'adunit', 'template': default_adunit_web_data},
            'ox_create_adunitgroup': {'object_type':'adunitgroup', 'template':default_adunitgroup_data},
            'ox_create_adv': {'object_type':'account', 'template':default_adv_data},
            'ox_create_audiencesegment': {'object_type':'audiencesegment', 'template':default_audiencesegment_data},
            'ox_create_conversiontag': {'object_type':'conversiontag', 'template':default_conversiontag_data},
            'ox_create_creative': {'object_type':'creative', 'template': default_creative_data},
            'ox_create_deal': {'object_type':'deal', 'template':default_deal_data},
            'ox_create_optimization': {'object_type': 'optimization', 'template': default_optimization_data},
            'ox_create_order': {'object_type': 'order', 'template': default_order_data},
            'ox_create_package': {'object_type': 'package', 'template': default_package_data},
            'ox_create_site': {'object_type': 'site', 'template': default_site_data},
            'ox_create_sitesection': {'object_type': 'sitesection', 'template': default_sitesection_data},
            'ox_create_user_net_admin': {'object_type': 'user', 'template': default_user_net_admin_data},
            'ox_create_user_net_report': {'object_type': 'user', 'template': default_user_net_report_data},
            'ox_create_user_pub_admin': {'object_type': 'user', 'template': default_user_pub_admin_data},
            'ox_create_user_pub_report': {'object_type': 'user', 'template': default_user_pub_report_data}
            }

def create_response(func):
    '''
    Description:
        Wrapper Function intended to decorate methods that simply call _create_object.
        Utilizes _create_object_beta for the time being.
        Utilizes mapper dict in order to correctly utilize parameters and pass correct arguments.
    '''
    def form_response(*args,**kwargs):
        temp_list_args = list(args)
        temp_list_args.insert(0,mapper[func.__name__]['object_type'])
        temp_list_args.insert(1,mapper[func.__name__]['template'])
        tuple_args = tuple(temp_list_args)
        return _create_object(*tuple_args, **kwargs)
    return form_response

def _create_object(object_type, default_data, input_data={}, expected_code=200, default=True, object_prefix=None, user_prefix=None, suffix=None, **kwargs):

    '''
    Description:
        1, handle input data: prefix & suffix name, combine default_data and input_data
        2, create object, using module ox_api_client
        3, input_data should be of type dict, and we can pass in additional key value pair of fields
           and have them been added to input_date. Example see comments on the top of page.
    '''

    logger.debug('*** Initial input_data:        %s' % input_data)
    logger.debug('*** Additional data passed in: %s' % kwargs)

    # if pass in additional data, for example,
    #         ox_create_li_pricedbid   order_uid=<uid>    status=Active    start_date=<date>
    # then the additional data will be appended to original passed in input_data (may be {})
    for k, v in kwargs.iteritems():
        # filter out key value pairs already in the arg list, for example: expected_code, suffix, etc.
        if k not in locals().keys():
            input_data[k] = v

    logger.debug('*** Combined input_data:       %s' % input_data)

    data = _data_init(object_type, default_data, input_data, default)
    inst = BuiltIn().get_library_instance('ox_api_client.OX_API_Client')
    response = client.ox_api_create_object_no_default(inst, object_type, data, object_prefix, user_prefix, suffix)

    if response['code'] != int(expected_code):
        if expected_code == 200:
            raise AssertionError('Error: %s creation failed, Msg: %s' % (object_type, response))
        else:
            raise AssertionError('Error: %s creation is not as expected, Msg: %s' % (object_type, response))
    return response


def _data_init(object_type, default_data, input_data={}, default=True, type_full=None):

    '''
    If default=True, combine default data and input data,
    The input data will override default data, if duplicate key
    '''
    if default:
        data = dict(default_data.items() + input_data.items())
    else:
        data = input_data

    # add in another layer of random suffix for name, to guarantee unique
    if 'name' in data:
        data['name'] += "_%s" % randint(10000000, 99999999)

    if 'email' in data:
        data['email'] = "%s_" % randint(10000000, 99999999) + data['email']

    return data


def _data_init_lineitem(li_default_data):
    '''
    Description: API requires that start_date needs to be now or in the future.
    '''
    date_in_a_month = str(datetime.utcnow() + timedelta(days = 30))[:-7]
    date_in_2_months = str(datetime.utcnow() + timedelta(days = 60))[:-7]
    li_default_data['start_date'] = date_in_a_month
    li_default_data['end_date'] = date_in_2_months
    return li_default_data


def _data_init_market(account_default_data):
    '''
    Description: API requires that for market active instances, network & publisher must have 'market' field
    '''
    inst = BuiltIn().get_library_instance('ox_api_client.OX_API_Client')
    response_session = client.ox_api_get(inst, url_session)
    if response_session['body_data']['instance']['market_active'] == '1':
        account_default_data['market'] = {}
    return account_default_data


@create_response
def ox_create_ad_html(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_ad_exch_html(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_ad_exch_ssrtb(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_adproduct(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_adunit_web(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_adunitgroup(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_adv(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return


def ox_create_adv_exch(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, ssrtb=False, **kwargs):
    '''
    ssrtb=False:   by default, we create an exchange advertiser without ssrtb, but we can turn it on
    '''
    object_type = 'account'

    if default:
        default_data = dict(default_adv_data.items() + default_adv_exch_data.items())
        if ssrtb:
            default_data = dict(default_data.items() + default_adv_exch_ssrtb_data.items())

    response = _create_object(object_type, default_data, input_data, expected_code,
                              default, object_prefix, user_prefix, suffix)

    return response

@create_response
def ox_create_audiencesegment(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_conversiontag(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_creative(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_deal(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return


def ox_create_li_exclusive(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):

    object_type = 'lineitem'
    default_data = _data_init_lineitem(default_li_exclusive_data)
    response = _create_object(object_type, default_data, input_data, expected_code,
                              default, object_prefix, user_prefix, suffix)
    return response


def ox_create_li_house(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):

    object_type = 'lineitem'
    default_data = _data_init_lineitem(default_li_house_data)
    response = _create_object(object_type, default_data, input_data, expected_code,
                              default, object_prefix, user_prefix, suffix)
    return response


def ox_create_li_net(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):

    object_type = 'lineitem'
    default_data = _data_init_lineitem(default_li_net_data)
    response = _create_object(object_type, default_data, input_data, expected_code,
                              default, object_prefix, user_prefix, suffix)
    return response


def ox_create_li_ng(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):

    object_type = 'lineitem'
    default_data = _data_init_lineitem(default_li_ng_data)
    response = _create_object(object_type, default_data, input_data, expected_code,
                              default, object_prefix, user_prefix, suffix)
    return response


def ox_create_li_preferred(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):

    object_type = 'lineitem'
    default_data = _data_init_lineitem(default_li_preferred_data)
    response = _create_object(object_type, default_data, input_data, expected_code,
                              default, object_prefix, user_prefix, suffix)
    return response


def ox_create_li_pricedbid(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):

    object_type = 'lineitem'
    default_data = _data_init_lineitem(default_li_pricedbid_data)
    response = _create_object(object_type, default_data, input_data, expected_code,
                              default, object_prefix, user_prefix, suffix, **kwargs)
    return response


def ox_create_li_sov(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):

    object_type = 'lineitem'
    default_data = _data_init_lineitem(default_li_shareofvoice_data)
    response = _create_object(object_type, default_data, input_data, expected_code,
                              default, object_prefix, user_prefix, suffix)
    return response


def ox_create_li_ssrtb(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):

    object_type = 'lineitem'
    default_data = _data_init_lineitem(default_li_ssrtb_data)
    response = _create_object(object_type, default_data, input_data, expected_code,
                              default, object_prefix, user_prefix, suffix)
    return response


def ox_create_li_vg(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):

    object_type = 'lineitem'
    default_data = _data_init_lineitem(default_li_volumegoal_data)
    response = _create_object(object_type, default_data, input_data, expected_code,
                              default, object_prefix, user_prefix, suffix)
    return response


def ox_create_network(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):

    object_type = 'account'
    default_data = _data_init_market(default_network_data)
    response = _create_object(object_type, default_data, input_data, expected_code,
                              default, object_prefix, user_prefix, suffix)
    return response

@create_response
def ox_create_optimization(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_order(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_package(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return


def ox_create_publisher(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):

    object_type = 'account'
    default_data = _data_init_market(default_pub_data)
    response = _create_object(object_type, default_data, input_data, expected_code,
                              default, object_prefix, user_prefix, suffix)
    return response


def ox_create_publisher_self_serve(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):

    object_type = 'account'
    default_data = _data_init_market(default_pub_self_serve_data)
    response = _create_object(object_type, default_data, input_data, expected_code,
                              default, object_prefix, user_prefix, suffix)
    return response

@create_response
def ox_create_site(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_sitesection(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_user_net_admin(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_user_net_report(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_user_pub_admin(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return

@create_response
def ox_create_user_pub_report(input_data={}, expected_code=200, object_prefix=None, user_prefix=None, suffix=None, default=True, **kwargs):
    return
