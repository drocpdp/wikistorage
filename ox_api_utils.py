#!/usr/bin/env python
from robot.api import logger
import ast

TYPES_UUID_DICT = {
    'account'         : 0xaccf,
    'user'            : 0xacc0,
    'accountrelationship' : 0xacc1,

    'site'            : 0xe000,
    'sitesection'     : 0xe001,
    'adunit'          : 0xe0ad,
    'adunitgroup'     : 0xe003,
    'audiencesegment' : 0xe004,
    'optimization'    : 0xf001,
    'adproduct'       : 0xf002,

    'conversiontag'   : 0xceaf,
    'creative'        : 0xceae,
    'order'           : 0xc001,
    'lineitem'        : 0xc002,
    'ad'              : 0xc0ad,
}


def v1_to_uuid(type, id, platform_hash):
    """
    Convert a API V1 id for an object to an equivalent UUID
    Argument: type - type of object, see TYPES_UUID_DICT above
              id   - id from API V1
              platform_hash - the instance containing the object
    Returns a UUID string
    """
    if not TYPES_UUID_DICT.has_key(type):
        raise Exception("UUID unknown type: " + type)

    if (id > 0xffffffff):
        raise Exception("UUID out of range id : " + str(id))

    if len(platform_hash) < 6:
        raise Exception("UUID platform hash to short: " + platform_hash)

    # API version
    api_version = 0xfff1

    # 16 bits, 8 bits for "clk_seq_hi_res",
    # 8 bits for "clk_seq_low",
    # two most significant bits holds zero and one for variant DCE1.1
    clk_data = 0x8123

    return '%08x-%04x-%04x-%04x-%s' % (id, TYPES_UUID_DICT[type],
                                       api_version, clk_data,
                                       platform_hash[-6:])

def uid_to_id(hex):
    """
    Convert a hex uid to int id
    Argument: hex - universal in that it can take the uid or id
    Returns the id
    """
    hex = str(hex)
    if len(hex) > 20:
        hex = hex.split("-")[0]
        hex = int(hex, 16)
        print hex
        return hex
    else:
        print hex
        return hex

def get_token_from_instance_hash(instance, user, instance_hash):
    """
    Get instance/user token
    Argument: instance       - instance name
              user           - user name
              instance_hash  - token dictionary
    Returns token or None
    """
    logger.debug('Get_Token_From_Instance_Hash %s, user %s' % (instance, user))
    logger.debug("Searching instance_hash:")
    logger.debug(instance_hash)

    if instance_hash == '':
        return None

    instance_hash = ast.literal_eval(instance_hash)
    if instance in instance_hash:
        token = [entry['token'] for entry in instance_hash[instance] if user == entry['user']]
        if len(token) >= 1:
            return token[0]
    logger.debug("ERROR: no token found")
    return None

def set_token_to_instance_hash(instance, user, token, instance_hash):
    """
    Set instance/user token
    Argument: instance       - instance name
              user           - user name
              token          - token to set
              instance_hash  - token dictionary
    Returns string of instance hash
    """
    new_entry = {'user':user, 'token':token}

    if instance_hash is '':
        instance_hash = {}
    else:
        instance_hash = ast.literal_eval(instance_hash)

    if not instance in instance_hash:
        instance_hash[instance] = list()

    instance_hash[instance].append(new_entry)

    return str(instance_hash)
