#!/usr/bin/env python

import cookielib
import fcntl
import oauth2 as oauth
import sys
import urllib
import urllib2
import urlparse
from robot.api import logger

HTTP_METHOD_OVERRIDES = ['DELETE', 'PUT']

class NoRedirectHandler(urllib2.HTTPRedirectHandler):
    """
    Class used to intercept redirects.
    """
    def http_error_302(self, req, fp, code, msg, headers):
        return

class OX_HTTP(object):
    """
    Class used to handle communication with HTTP servers.
    Has methods to handle get, post, and delete methods.
    Also has methods for login to sso server, as well as to validate
    an SSO token against an API server.
    """

    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    def __init__(self):
        """
        Set up a private cookie jar, and a private SSO consumer property
        to store a potential oauth.Consumer object.
        """
        self._consumer = None
        self.token = None
        self.sso_realm = None
        self.lock_file_object = None

        self._cookie_jar = cookielib.LWPCookieJar()
        self.opener = \
            urllib2.build_opener(urllib2.HTTPCookieProcessor(self._cookie_jar))

    ############################
    # Private methods
    ############################
    def _authorize_token(self, email, password):
        """
        Helper method to authorize.
        """
        data = {
            'email': email,
            'password': password,
            'oauth_token': self.token.key}

        res = self._request(
                url=self.sso_authorization_url,
                method='POST',
                data=data,
                sign=True)

        res_read = res.read()
        verifier = urlparse.parse_qs(res_read)['oauth_verifier'][0]
        self.token.set_verifier(verifier)

    def _encode_dict(self, dict_input):
        new_dict = {}
        for key, value in dict_input.items():
            if isinstance(value, unicode):
                value = value.encode('utf-8')
            elif isinstance(value, str):
                # Must be encoded in UTF-8
                value.decode('utf-8')
            new_dict[key] = value
        return new_dict

    def _fetch_access_token(self):
        """
        Helper method to fetch and set access token.
        Returns token string.
        """
        res = self._request(url=self.sso_access_token_url, method='POST',
                            sign=True)
        self.token = oauth.Token.from_string(res.read())
        return self.token

    def _fetch_request_token(self):
        """
        Helper method to fetch and set request token.
        Returns token string.
        """
        res = self._request(url=self.sso_request_token_url, method='POST',
                            sign=True)
        res_str = res.read()
        self.token = oauth.Token.from_string(res_str)
        return self.token

    def _parse_ox_http_response(self,response,verbose=0):
        """
        Parse the HTTP response returned by ox_http_get, ox_http_post,
        etc...  which is a urllib2 object, and return a dictionary containing
        the code, msg, header, body, and the response object.
        The code, msg, header, and body can be used by Robot.
        The response object can be used by other Python modules.
        """
        code = response.code
        msg = response.msg
        src = response.geturl()
        headers = response.info().headers
        response_body = response.read()

#        logger.debug('*** Response code:       %s' % code)
#        logger.debug('*** Response msg %s' % msg)
#        logger.debug('*** Response src %s' % src)
#        logger.debug('*** Response headers %s' % headers)
        if verbose:
            logger.debug('*** Response body:       %s' % response_body)
        if self.sso_realm:
            logger.debug("*** sso realm:           %s" % self.sso_realm)

        return {"code": code, "msg": msg, "headers": headers, "src": src,
                "body": response_body, "response": response}

    def _request(self, url, method='GET', headers={}, data=None, sign=False):
        """
        Helper method to make a (optionally OAuth signed) HTTP request.
        """

        self.ox_http_lock()

        # Since we are using a urllib2.Request object we need to assign a value
        # other than None to "data" in order to make the request a POST request,
        # even if there is no data to post.
#        logger.debug("Requesting URL: %s" % url)
        if method == 'POST' or method == "PUT":
            data = data if data else ''

        req = urllib2.Request(url, headers=headers, data=data)

        # We need to set the request's get_method function to return a HTTP
        # method for any values other than GET or POST.
        if method in HTTP_METHOD_OVERRIDES:
            req.get_method = lambda: method

        if sign:
            req = self._sign_request(req)

        # Stringify data.
        if (data and isinstance(data, dict)):
            # We encode the data here but we are not setting the charset
            # on the Content-type header on the request. urllib2 does not
            # set the charset on Content-type and things seem to work ok.
            # We may need to revisit this if we see any problems.
            data_encoded = self._encode_dict(req.get_data())
            req.add_data(urllib.urlencode(data_encoded))

        # urllib2 will throw exception when response code is in the 400 range
        try:
#            logger.debug(data)
            resp = self.opener.open(req)
#            logger.debug("Request Headers:")
#            logger.debug(req.unredirected_hdrs)
#            logger.debug(req.header_items())
        except urllib2.HTTPError, error:
            resp = error
            logger.debug("Request Headers:")
            logger.debug(req.unredirected_hdrs)
            logger.debug(req.header_items())
            logger.debug(resp)
            logger.debug(dir(resp))
            logger.debug(resp.code)
            logger.debug(resp.msg)
            logger.debug(resp.strerror)
            logger.debug(resp.info().headers)
        except urllib2.URLError, error:
            resp = error
            logger.debug("Request Headers:")
            logger.debug(req.unredirected_hdrs)
            logger.debug(req.header_items())
            logger.debug(dir(resp))
            logger.debug(resp.errno)
            logger.debug(resp.filename)
            logger.debug(resp.reason)
            logger.debug(resp.message)
            logger.debug(resp.strerror)
        except:
            logger.debug("Request Headers:")
            logger.debug(req.unredirected_hdrs)
            logger.debug(req.header_items())
            logger.debug("Unexpected error: %s" % sys.exc_info()[0])
            raise

        self.ox_http_unlock()
        return resp

    def _sign_request(self, req):
        """
        Utility method to sign a request.
        Input: req - Request object to be signed
        Return: req - Signed request
        """

        parameters = {'oauth_callback': self.sso_callback_url}
        headers = req.headers
        data = req.data

        # Add any (POST) data to the parameters to be signed in the OAuth
        # request.
        if data:
            parameters.update(data)

        # Create a temporary oauth2 Request object and sign it so we can steal
        # the Authorization header.
        oauth_req = oauth.Request.from_consumer_and_token(
            consumer=self._consumer,
            token=self.token,
            http_method=req.get_method(),
            http_url=req.get_full_url(),
            parameters=parameters,
            is_form_encoded=True)

        oauth_req.sign_request(
            oauth.SignatureMethod_HMAC_SHA1(),
            self._consumer,
            self.token)

        # Update our original requests headers to include the OAuth
        # Authorization header and return it.
        req.headers.update(oauth_req.to_header(realm=self.sso_realm))
        return \
            urllib2.Request(req.get_full_url(), headers=req.headers, data=data)

    def _sso_settings(self, sso):
        """
        Set up instance variables used for request signature and SSO
        authentication.
        Input: sso - A dictionary containing SSO credentials, with the
                     following keys: site, realm, consumer_key, consumer_secret
        """
        sso_site = sso['site']
        self.sso_realm = sso['realm']
        self.sso_callback_url = 'oob'
        self.sso_consumer_key = sso['consumer_key']
        self.sso_consumer_secret = sso['consumer_secret']

        self.sso_request_token_url = sso_site \
                                       + '/api/index/initiate'
        self.sso_access_token_url = sso_site \
                                      + '/api/index/token'
        self.sso_authorization_url = sso_site \
                                       + '/login/process'

    ############################
    # HTTP methods
    ############################
    def ox_http_lock(self, filename="/tmp/pa.lock"):
        """
        Open file and lock
        """
#        logger.debug('*** OX API Lock filename %s' % filename)
        self.lock_file_object = open(filename, "w+")
        try:
            rv = fcntl.lockf(self.lock_file_object, fcntl.LOCK_EX)
        except Exception as e:
            logger.debug('*** OX API Lock exception!')
            logger.debug(e)

    def ox_http_unlock(self):
        """
        Unlock and close file
        """
#        logger.debug('*** OX API Unlock')
        rv = fcntl.lockf(self.lock_file_object, fcntl.LOCK_UN)
        self.lock_file_object.close()

    def ox_http_delete(self, url, headers={}, verbose=1, obj_data=None):
        """
        Send a DELETE message to the HTTP server
        Args: url - The URL relative to http://<api-hostname><api-path>
        """
        logger.debug("****************************************")
        logger.debug('*** OX API Delete url:   %s' % url)

        res = self._request(url, method='DELETE', headers=headers, data=obj_data)

        response = self._parse_ox_http_response(res, verbose)
        return response

    def ox_http_get(self, url, headers={}, verbose=1):
        """
        Send a GET message to the HTTP server
        Args: url - The URL relative to http://<api-hostname><api-path>
        """
        logger.debug("****************************************")
        logger.debug('*** OX API Get url:      %s'       % url)
        res = self._request(url, method='GET', headers=headers)
        response = self._parse_ox_http_response(res, verbose)
        return response

    def ox_http_get_no_redirect(self, url, headers={}, verbose=1):
        """
        Send a GET message to the HTTP server
        Args: url - The URL relative to http://<api-hostname><api-path>
        """
        logger.debug('*** OX API Get No Redirect url %s' % url)
        self.opener = \
            urllib2.build_opener(NoRedirectHandler(), urllib2.HTTPCookieProcessor(self._cookie_jar))
        res = self._request(url, method='GET', headers=headers)
        self.opener = \
            urllib2.build_opener(urllib2.HTTPCookieProcessor(self._cookie_jar))
        response = self._parse_ox_http_response(res, verbose)
        logger.debug("RESPONSE HEADERS::: %s" % response['headers'])
        return response

    def ox_http_get_cookies(self, url, headers={}, verbose=1):
        """
        Return cookies after sending GET message to the HTTP server
        Args: url - The URL relative to http://<api-hostname><api-path>
        """
        logger.debug('*** OX API Get Cookies url %s' % url)
        self.ox_http_get(url, headers=headers)
        cookie_hash = dict((cookie.name, cookie.value) for cookie in self._cookie_jar)
        return cookie_hash

    def ox_http_get_reject_cookies(self, url, headers={}, verbose=1):
        """
        Send a GET message without allowing cookies
        Args: url - The URL relative to http://<api-hostname><api-path>
        """
        logger.debug('*** OX API Get Reject Cookies url %s' % url)
        self.opener = urllib2.build_opener()
        response = self.ox_http_get(url, headers=headers)
        self.opener = \
            urllib2.build_opener(urllib2.HTTPCookieProcessor(self._cookie_jar))
        return response

    def ox_http_post(self, url, data=None, headers={}, verbose=1):
        """
        Send a POST message to the HTTP server
        Args: url - The URL relative to http://<api-hostname><api-path>
              data - A dictionary containing the POST data
        """
        logger.debug("****************************************")
        logger.debug('*** OX API Post url:     %s' % url)
        logger.debug('*** OX API Post data:    %s' % data)

        res = self._request(url, method='POST', headers=headers, data=data)
        response = self._parse_ox_http_response(res, verbose)
        return response

    def ox_http_put(self, url, data=None, headers={}, verbose=1):
        """
        Send a PUT message to the HTTP server
        Args: url - The URL relative to http://<api-hostname><api-path>
              data - A dictionary containing the PUT data
        """
        logger.debug("****************************************")
        logger.debug('*** OX API Put url:      %s' % url)
        logger.debug('*** OX API Put data:     %s' % data)

        res = self._request(url, method='PUT', headers=headers, data=data)
        response = self._parse_ox_http_response(res, verbose)
        return response

    ############################
    # SSO methods
    ############################
    def ox_http_sso_login(self, email_address, password='Testing123',
                          sso_credentials={}):
        """
        Authenticate the user via SSO. And set up the access_token for the
        object for subsequent API calls.
        Args: email_address
              password - Mostly 'Testing123' for now
              sso_credentials - A dictionary containing the following keys:
                 consumer_key, consumer_secret, realm, callback_url
        Returns: Nothing
        """

        logger.debug('*** OX API Session Login: %s' % email_address)
        logger.debug('***              Settings: %s' % sso_credentials)

        self._sso_settings(sso_credentials)

        # Step 0. Initiate an Oauth consumer object
        self._consumer = oauth.Consumer(self.sso_consumer_key,
                                        self.sso_consumer_secret)

        # Step 1. Fetch temporary request token.
        self._fetch_request_token()

        # Step 2. Log in to SSO server and authorize token.
        self._authorize_token(email_address, password)

        # Step 3. Swap temporary request token for permanent access token.
        # If you need to store the access token yourself you can do so with
        # something similar to:
        #   token_str = self._fetch_access_token()
        #   access_token = urlparse.parse_qs(token_str)['oauth_token'][0]
        # ---HDL--- Line above does not work. I found the following to work.
        #   access_token = token_str.key
        self._fetch_access_token()

        # Step 4. Validate your access token.
        # You'll more than likely want to call the validate_session method,
        # with a URL to the API server that will validate your token.
        # The API client should make this call separately as it is a function
        # of the API server. A helper method ox_http_validate_session is
        # provided here for that.

    def ox_http_sso_logout(self, domain):
#       self._cookie_jar.clear(domain)
        self._cookie_jar.clear()
        self.token = None

    def ox_http_clear_cookies(self):
        self._cookie_jar.clear()

    def ox_http_validate_session(self, validate_url, cookie):
        """
        Validate an API session.
        Input: validate_url - URL to the API server to validate the token
               cookie - cookielib object for the HTTP session
        """
        logger.debug('OX Validate Session: %s' % validate_url)
        res = self.ox_http_put(validate_url, verbose=0)
        return res['body']

    def ox_http_validate_session_user(self, validate_url, cookie):
        """
        Validate an API session on the user level for v2 or OX4.
        Input: validate_url - URL to the API server to validate the token
               cookie - cookielib object for the HTTP session
        """
        logger.debug('OX Validate Session: %s' % validate_url)
        res = self.ox_http_get(validate_url, verbose=0)
        return res['code']
