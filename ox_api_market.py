#!/usr/bin/env python

#  This module deal with api inputs for market related objects,
#  including:   1,  optimization (market selling rules)
#               2,  market field (inside of network and publisher)


market_operator_ids = ["AUTO-CA", "OX", "OX-AUTO", "OX-DE", "OX-DEMO",
                       "OX-GB", "OX-GRID", "OX-LMI", "OX-MOB", "OX-PRE",
                       "OX-SSP", "OX-V2", "OXM-SSP", "cci", "cci-sea"]


def set_market_filters(action="block_all", input={}):
    '''
    This function set optimization.market.filters field.

    The most simple case will be 'block_all' or 'allow_all'

    If action is not simple block_all or allow_all, we allow input.
    input should be in this format:
          {'OX-GB':
              {"ids": {"123": true,"456": true},
              "op": "allow",
              "region": "OX-GB"}
           'OX':
              {  }
           }
    '''
    allowed_actions = ["block_all", "allow_all",
                       "block_except", "allow_except"]

    if action not in allowed_actions:
        raise ValueError("input action is wrong")

    filters = dict()
    if action in ["allow_all", "block_all"]:
        for id in market_operator_ids:
            filters[id] = { "ids": None,
                            "op": action,
                            "region": id }
    else:
        for id in market_operator_ids:
            if id not in input:
                action_on_others = action.split('_')[0] + "_all"
                filters[id] = { "ids": None,
                                "op": action_on_others,
                                "region": id}
            else:
                filters[id] = input[id]

    return {"filters": filters}
