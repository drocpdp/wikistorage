#!/usr/bin/env python

import cookielib
import urllib
import urlparse
from robot.api import logger

import oxbot_utils
import ox_http
import ox_json

APIv1_PATH = '/ox/3.0/a'
APIv2_PATH = '/ox/4.0'
# when instance data uses the apiv2 with no router:  APIv2_PATH = ''
APIv1_HOST_KEY = 'api_v1'
APIv2_HOST_KEY = 'api_v2'

class OX_API_Client(object):
    """
    Interface class for interaction with OX3 API.

    This class manages input and output data for the API objects.
    It uses ox_http.OX_HTTP for communicating with the
    OX3 API server at the HTTP level.

    Usage:
       client = ox_api_client.OX_API_Client()
       client.ox_api_login(email_address, password, instance_name)
       object_data = client.find_object('account', 'name', 'Robot Test')
       robot_test_id = object_data['obj_id']
    """

    API_PATH = ''
    API_HOST_KEY = ''

    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    def __init__(self, my_ox3v2="True"):
        """
        The OX_HTTP object will keep some of the data as attributes
        for use in subsequent HTTP calls.
        """

        if my_ox3v2 == "False" or my_ox3v2 == "false" or my_ox3v2 == False:
            self.ox3v2 = False
            OX_API_Client.API_PATH = APIv1_PATH
            OX_API_Client.API_HOST_KEY = APIv1_HOST_KEY
        else:
            self.ox3v2 = True
            OX_API_Client.API_PATH = APIv2_PATH
            OX_API_Client.API_HOST_KEY = APIv2_HOST_KEY
        self.ox_http_session = ox_http.OX_HTTP()
        self.email = None

    def _get_default_http_data(self, object_type):
        """
        Default post data is the data required for a create or update
        action on an object. The templates for the required data are stored
        in external data file. Each operation will override these default
        data with their own data as needed.
        """
        if self.ox3v2:
            data_api_path = 'data/api/'
        else:
            data_api_path = 'data/api-v1/'
        data_filename = data_api_path + object_type + '/creation_data.json'
        http_data = ox_json.ox_json_from_file(data_filename)
        return http_data

    def _overlay_with(self, http_data, object_data={}):
        """
        Post data and object data are dictionaries.
        Entries in post data will be used as default for object_data
        Returns adjusted object_data
        """
        for key in http_data.keys():
            if key not in object_data.keys():
                object_data[key] = http_data[key]
        return object_data

    def _parse_ox_api_response(self,response):
        """
        Parses the HTTP response returned by ox_http.OX_HTTP,
        which is a dictionary containing code, msg, headers, body, and a
        urllib2 response object.
        Returns a dictionary containing code, msc, headers, body, and
        body_data (which is a structure represented by the json string in the
        body), and obj_id.
        The obj_id is set if the json data implies such data, i.e.
          if the data is a dictionary with an 'id' key
          if the data is a list and contains only an item with just digits
          if the data is a list and contains only a dictionay with an 'id' key
        """
        code = response['code']
        msg = response['msg']
        headers = response['headers']
        response_body = response['body']
        if response_body and code != 502:
            body_data = ox_json.ox_json_from_string(response_body)
        else:
            body_data = None

        obj_id = None
        obj_uid = None
        if body_data:
            obj_data = body_data
            if isinstance(obj_data, dict) and 'objects' in obj_data:
                obj_data = obj_data['objects']
            if isinstance(obj_data, list) and obj_data.__len__() == 1:
                obj_data = obj_data[0]

            if isinstance(obj_data, dict):
                if 'id' in obj_data and _can_be_a_number(obj_data['id']):
                    obj_id = int(obj_data['id'])
                if 'uid' in obj_data:
                    obj_uid = obj_data['uid']
            elif _can_be_a_number(obj_data):
                obj_id = int(obj_data)

        logger.debug('*** Response code:       %s' % code)
        logger.debug('*** Response obj_id:     %s' % obj_id)
        logger.debug('*** Response obj_uid:    %s' % obj_uid)
        logger.debug('*** Request by email:    %s' % self.email)
        logger.debug("****************************************")

        return {"code": code, "obj_id": obj_id, "obj_uid": obj_uid,
                "body_data": body_data, "msg": msg,
                "headers": headers, "body": response_body}

    def _resolve_url(self, url):
        """"""
        parse_res = urlparse.urlparse(url)
        if not parse_res.scheme:
            scheme = self.session['scheme']
            host = self.session['domain']
            if self.session['port']:
                host = host + ":" + self.session['port']
            api_path = self.session['api_path']
            url ='%s://%s%s%s' % (scheme, host, api_path, parse_res.path)
            url = url + '?' + parse_res.query if parse_res.query else url

        return url

    def ox_api_session(self, instance_name='instance7'):
        """
        Setting up a session setting dictionary to pass into OX_HTTP,
        based on instance_name. The setting is looked up in the instance data
        This setting is used as default for short-handed API calls, i.e.
        calls without the http, or domain parts.
        """
        _instance_file = oxbot_utils.ox_utils_file('data/instance/' \
                                                   + instance_name + '.json')
        _file = open(_instance_file)
        _instance_data = ox_json.ox_json_from_string(_file.read())

        # We need to handle the domain and port number, since the host
        # definition may include the port number
        port = None
        domain = _instance_data['hosts'][OX_API_Client.API_HOST_KEY]
        if domain.find(':') > 0:
            (domain, port) = domain.split(':')
            port_specified = True

        session = {}
        session['domain'] = domain
        session['port'] = port
        session['scheme'] = 'http'
        session['api_path'] = OX_API_Client.API_PATH

        self.session = session
        return _instance_data

    def ox_api_login(self, email_address, password, instance_name='instance7'):
        """
        Initiate an OX3 API client login session
        The HTTP client session will have the openx3_access_token cookie
        """

        self.email = email_address
        instance_data = self.ox_api_session(instance_name)
        self.ox_http_session.ox_http_sso_login(email_address, password,
                                                 instance_data['sso'])
        port_specified = False
        if self.session['port']:
            port_specified = True

        # We need to store our access token as the openx3_access_token cookie.
        # This cookie will be passed to all future API requests via the
        # self.ox_http_session object.

        cookie = cookielib.Cookie(
            version=0,
            name='openx3_access_token',
            value=self.ox_http_session.token.key,
            port=self.session['port'],
            port_specified=port_specified,
            domain=self.session['domain'],
            domain_specified=True,
            domain_initial_dot=False,
            path='/',
            path_specified=True,
            secure=False,
            expires=None,
            discard=False,
            comment=None,
            comment_url=None,
            rest={})
        self.ox_http_session._cookie_jar.set_cookie(cookie)
        logger.debug("openx3_access_token COOKIE: %s" % cookie)
        print ("openx3_access_token COOKIE: %s" % cookie)

        if not self.ox3v2:
            self.ox_http_session.ox_http_validate_session(
                    self._resolve_url('/session/validate'), cookie)

    def ox_api_switch_instance(self, token, instance_name='instance7'):
        """
        Initiate an OX3 API client login session
        Set the HTTP client session to the openx3_access_token with the token passed in
        """
        instance_data = self.ox_api_session(instance_name)

        port_specified = False
        if self.session['port']:
            port_specified = True

        # We need to store our access token as the openx3_access_token cookie.
        # This cookie will be passed to all future API requests via the
        # self.ox_http_session object.

        cookie = cookielib.Cookie(
            version=0,
            name='openx3_access_token',
            value=token,
            port=self.session['port'],
            port_specified=port_specified,
            domain=self.session['domain'],
            domain_specified=True,
            domain_initial_dot=False,
            path='/',
            path_specified=True,
            secure=False,
            expires=None,
            discard=False,
            comment=None,
            comment_url=None,
            rest={})
        self.ox_http_session._cookie_jar.set_cookie(cookie)
        logger.debug("openx3_access_token COOKIE: %s" % cookie)
        if not self.ox3v2:
            self.ox_http_session.ox_http_validate_session(
                    self._resolve_url('/session/validate'), cookie)
        else:
            code = self.ox_http_session.ox_http_validate_session_user(
                        self._resolve_url('/user'), cookie)
            logger.debug("User session response code: %s" % code)

    def ox_api_validate_session_user(self):
        for cookie in self.ox_http_session._cookie_jar:
            code = self.ox_http_session.ox_http_validate_session_user(
                        self._resolve_url('/user'), cookie)
            logger.debug("User session response code: %s" % code)

    def ox_api_get_login_token(self):
        return self.ox_http_session.token.key

    def ox_api_logout(self):
        """
        Terminate an OX3 API client session
        """
        if not self.ox3v2:
            self.ox_http_session.ox_http_delete(self._resolve_url('/session'))
        self.ox_http_session.ox_http_sso_logout(self.session['domain'])

    def ox_api_find_object(self, object_type, object_data={},
                           object_prefix=None, user_prefix=None, suffix=None):
        """
        Find an object by matching all key/value pairs of the input hash.
        Unless overridden by a 'qualifiers' key, the listing defaults to
        '&overload=medium.
        For a 'name' index, if either object_prefix or user_prefix are
        provided, prepend them to the 'name' value.  Returns a dictionary.
        Example for API v1:
            /site?index1=key&value1=value&overload=medium
        Example for API v2:
            /site?key=value
        [Rf. _parse_ox_api_response(). ]
        """
        ctr=1
        quals="overload=medium"
        query_args = []
        for key,value in object_data.items():
           sctr = str(ctr) + "="
           if key == "qualifiers":
              quals = value
           else:
              if key == 'name':
                  value = oxbot_utils.ox_utils_prefix_suffix_name(value,
                                         object_prefix, user_prefix, suffix)
              if not self.ox3v2:
                  arg = ("index"  + sctr + key +
                         "&value" + sctr + urllib.quote_plus(value))
              else:
                  arg = key + "=" + urllib.quote_plus(value)

              ctr = ctr + 1
              query_args.append(arg)

        query_arguments = ""
        if query_args:
           query_arguments = "&".join(query_args)
        if quals:
           if query_arguments:
              query_arguments = query_arguments + "&" + quals
           else:
              query_arguments = quals

        url= "/" + object_type + "?" + query_arguments + "&" + "limit=500"

        response = self.ox_http_session.ox_http_get(self._resolve_url(url))
        response_data = self._parse_ox_api_response(response)
        return response_data

    def ox_api_create_object(self, object_type, object_data={},
                              object_prefix=None, user_prefix=None,
                              suffix=None, default=True):
        """
        Create an object. Default http_data will be retrieve from the
        data area. Data in object_data will overwrite those in default
        post data.
        If object_prefix is provided, prepend it to the value for 'name'.
        If user_prefix is provided, prepend it also.
        Returns a dictionary. See _parse_ox_api_response()
        """
        url = "/" + object_type

        if default:
            http_data = self._get_default_http_data(object_type)
            http_data = self._overlay_with(http_data, object_data)
        else:
            http_data = object_data

        if 'name' in http_data:
            http_data['name'] = oxbot_utils.ox_utils_prefix_suffix_name(
                                              http_data['name'],
                                              object_prefix,
                                              user_prefix,
                                              suffix)

        if self.ox3v2:
            http_data = ox_json.ox_json_dump(http_data)
            headers = {'Content-Type': 'application/json',
                       'Content-Length': len(http_data)}
        else:
            headers = {}

        response = self.ox_http_session.ox_http_post(self._resolve_url(url),
                                                     http_data, headers=headers)
        response_data = self._parse_ox_api_response(response)
        return response_data

    def ox_api_create_object_no_default(self, object_type, object_data={},
                              object_prefix=None, user_prefix=None,
                              suffix=None):
        """
        This method is very similar to ox_api_create_object, just that this method
        does not read from the default creation data file
        """
        return self.ox_api_create_object(object_type, object_data, object_prefix, user_prefix, suffix, default=False)


    def ox_api_update_object(self, object_type, object_id, object_data={},
                             object_prefix=None, user_prefix=None,
                             suffix=None):
        """
        Update an object. All data needed for the update should be provided
        in object_data, as a dictionary.
        If object_prefix is provided, prepend it to the value for 'name'.
        If user_prefix is provided, prepend it also.
        In OX API V1, POST is used for updates; in V2, PUT is used.
        Argument: object_type - name of object, e.g. account (as in /account)
                  object_id - numeric id, e.g. 123456 (as in /account/123456)
                  object_data - {"status": 'Active'}
        Returns a dictionary. See _parse_ox_api_response()
        """
        if object_id:
            url = "/" + object_type + '/' + object_id.__str__()
        else:url = "/" + object_type

        http_data = object_data
        if 'name' in http_data:
            http_data['name'] = oxbot_utils.ox_utils_prefix_suffix_name(
                                                http_data['name'],
                                                object_prefix,
                                                user_prefix,
                                                suffix)

        if self.ox3v2:
            http_data = ox_json.ox_json_dump(http_data)
            headers = {'Content-Type': 'application/json',
                       'Content-Length': len(http_data)}
            response = self.ox_http_session.ox_http_put(
                                            self._resolve_url(url),
                                            http_data, headers=headers)
        else:
            response = self.ox_http_session.ox_http_post(
                                            self._resolve_url(url), http_data)

        response_data = self._parse_ox_api_response(response)
        return response_data

    def ox_api_delete_object(self, object_type, object_id=None, delete_options=None, obj_data=None):
        """
        Delete an object.
        Argument: object_type - name of object, e.g. account (as in /account)
                  object_id - numeric id, e.g. 123456 (as in /account/123456)
                  delete_options - e.g. hard=1
        Returns a dictionary. See _parse_ox_api_response()
        """
        if object_id:
            url = "/" + object_type + '/' + object_id.__str__()
        else:
            url = "/" + object_type

        if delete_options:
            url = url + '?' + delete_options

        if self.ox3v2:
            headers = {'Content-Type': 'application/json'}
        else:
            headers = {}

        obj_data = ox_json.ox_json_dump(obj_data) if isinstance(obj_data,list) else obj_data

        response = self.ox_http_session.ox_http_delete(self._resolve_url(url), headers=headers, obj_data=obj_data)
        response_data = self._parse_ox_api_response(response)
        return response_data

    def ox_api_delete_object_by_name(self, object_type, object_name,
                                     delete_options=None,
                                     object_prefix=None,
                                     user_prefix=None,
                                     suffix=None):
        """
        Delete object fitting a given name. Will do an API listing for the
        object type based on the name. Will not delete anything if more
        than one object are returned from the listing. There are 2 reasons
        for this:
           1. To prevent inadvertent deletion.
           2. If more than one objects are deleted and one is a parent
              of the other, we need to delete the child first.
        Argument: object_type - type of object, e.g. account (as in /account)
                  object_name - a given name to look up the objects
                  delete_options - e.g. hard=1
        Returns a dictionary. See _parse_ox_api_response()
            One of the following 3 responses will be returned:
            a. response from the API listing returning zero object
            b. response from the API listing returning more than 1 object
            c. response from the last API delete
        """
        request = {}
        request['name']=object_name
        response = self.ox_api_find_object(object_type, request,
                                           object_prefix, user_prefix, suffix)
        # If response['obj_id'] is null (can't find any object matching name)
        # or returning more than one object, do not attempt the deletion
        if not response['obj_id'] or response['body_data'].__len__() > 1:
            return response

        object_id = response['body_data'][0]['id']
        logger.debug("Deleting %s %s" % (object_type, object_id))
        response_delete = self.ox_api_delete_object(object_type,
                                                    object_id,
                                                    delete_options)
        return response_delete

    def ox_api_list_object(self, object_type, object_id, list_options=None):
        """
        Listing content of an object.
        Argument: object_type - name of object, e.g. account (as in /account)
                  object_id - numeric id, e.g. 123456 (as in /account/123456)
                  list_options - e.g. with-associations=1&overload=medium
        Returns a dictionary. See _parse_ox_api_response()
        """
        url = "/" + object_type + '/' + object_id.__str__()
        if list_options:
            url = url + '?' + list_options
        response = self.ox_http_session.ox_http_get(self._resolve_url(url))
        response_data = self._parse_ox_api_response(response)
        return response_data

    def ox_api_get(self, url):
        """
        Interface to ox_http.ox_http_get()
        """
        response = self.ox_http_session.ox_http_get(self._resolve_url(url))
        response_data = self._parse_ox_api_response(response)

        return response_data

    def ox_api_post(self, url, http_data=None):
        """
        Interface to ox_http_session.ox_http_post()
        """
        print self._resolve_url(url)
        response = self.ox_http_session.ox_http_post(self._resolve_url(url),
                                                     http_data)
        response_data = self._parse_ox_api_response(response)
        return response_data

    def ox_api_put(self, url, http_data):
        """
        Interface to ox_http_session.ox_http_put()
        """

        http_data = ox_json.ox_json_dump(http_data)
        headers = {'Content-Type': 'application/json',
                   'Content-Length': len(http_data)}

        response = self.ox_http_session.ox_http_put(self._resolve_url(url),
                                                http_data, headers=headers)
        response_data = self._parse_ox_api_response(response)
        return response_data

    def ox_api_delete(self, url):
        """
        Interface to ox_http_session.ox_http_delete()
        """
        response = self.ox_http_session.ox_http_delete(self._resolve_url(url))
        response_data = self._parse_ox_api_response(response)
        return response_data

def _can_be_a_number(number):
    if isinstance(number, int):
        return True
    elif (isinstance(number, basestring) and number.isdigit()):
        return True
    return False
