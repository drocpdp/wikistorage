
#  Documentation:

#  ox_assert_include_exclude(input_string, input_dict):
#           input a string and a dictionary,
#           test if the string includes or excludes the keys in the dict
#  ox_generate_random_number(min, max):
#           generate a random number in range of [min, max], inclusive
#  ox_get_bool(input):
#           return True for input: 1/0/int/True,  False otherwise
#  ox_get_random_item_from_list(mylist):
#  ox_sort_list(list_in, ignore_case=True):
#  ox_str_to_lower(mystr):
#  ox_str_to_upper(mystr):
#  ox_str_should_contain(str1, str2, ignore_case=True):


import random


#-------------------------------------------------------------------------------
def ox_assert_include_exclude(input_string, input_dict):
#-------------------------------------------------------------------------------
    '''
    Input:  input_string:  can be basically any type, will be converted to string
            input_dict:    should be of type dictionary, example: { 'uid1': True, 'uid2': False }
    Description:           Assert the input_string include or exclude some strings in input_dict.
                           For above example, will assert input_string includes uid1, excludes uid2
    '''

    input_string = str(input_string)
    if not isinstance(input_dict, dict):
        raise AssertionError('Error: input_dict must be of type dict, not %s' % type(input_dict))

    for k, v in input_dict.iteritems():
        if v and ( not k in input_string ):
            raise AssertionError('Error: %s does not include %s, while we expect otherwise' % (input_string, k))
        elif not v and ( k in input_string ):
            raise AssertionError('Error: %s includes %s, while we expect otherwise' % (input_string, k))

    return


#-------------------------------------------------------------------------------
def ox_generate_random_number(min, max):
#-------------------------------------------------------------------------------
    """
    input: min, max:  should be integers, or strings consist of integers
    returns a random integer number, within [min, max], inclusively.
    """
    try:
        min = int(min)
        max = int(max)
    except TypeError:
        print "min and max must be integers or strings consist of integers"

    return random.randint(min,max)


#-------------------------------------------------------------------------------
def ox_generate_random_number_str(min, max):
#-------------------------------------------------------------------------------
    """
    input: min, max:  should be integers, or strings consist of integers
    returns a random integer number in string format, within [min, max], inclusively.
    """
    return str(ox_generate_random_number(min, max))


#-------------------------------------------------------------------------------
def ox_get_bool(input):
#-------------------------------------------------------------------------------
    '''
    Input:   0/1/int:        integer or string, or
             None/Null:      NoneType or string, or
             True/False:     string or boolean
    Return:  boolean True,   if input: 1/True/any number
             boolean False,  if input: 0/False/None/Null
    '''
    if isinstance(input, int) or isinstance(input, bool) or (input is None):
        my_bool = bool(input)
    elif isinstance(input, basestring) and input.isdigit():
        my_bool = bool(int(input))
    elif isinstance(input, basestring) and (input.lower() in ('none', 'null')):
        my_bool = False
    else:
        raise ValueError('Input must be 0/1/Integer/True/False/None')

    return my_bool


#-------------------------------------------------------------------------------
def ox_get_random_item_from_list(mylist):
#-------------------------------------------------------------------------------
    """
    input:   a list of items, could be strings or numbers
    output:  randomly pick one item and return it, in original type
    """
    if not isinstance(mylist, list) or len(mylist) == 0:
        raise ValueError("Input must be a list, not a single item, not empty")
    else:
        return random.choice(mylist)

#-------------------------------------------------------------------------------
def ox_sort_list(list_in, ignore_case=True, asc=True):
#-------------------------------------------------------------------------------
    """
    Given a list of strings, return a list sorted alphabetically,
    ascending or descending(asc, desc).

    Example input: [a, A, b, B, z].
    Example output:
            if ignore_case is True,  return [a, A, b, B, z], most of our API test need this.
            if ignore_case is False, return [A, B, a, b, z]
    """

    # the ignore_case from input might be bool/string/int, convert to bool:
    ignore_case = ox_get_bool(ignore_case)

    # if input is unicode list, convert to string:
    list_str = []
    for item in list_in:
        if isinstance(item, unicode):
            list_str.append(item.encode('ascii', 'replace'))
        else:
            list_str.append(item)

    sorted_list = sorted(list_str, key=str.upper) if ignore_case else sorted(list_str)
    if not asc:
        sorted_list.reverse()
    return sorted_list

#-------------------------------------------------------------------------------
def ox_str_to_lower(mystr):
#-------------------------------------------------------------------------------
    """
    return the lower case string
    """
    return mystr.lower()

#-------------------------------------------------------------------------------
def ox_str_to_upper(mystr):
#-------------------------------------------------------------------------------
    """
    return the upper case string
    """
    return mystr.upper()


#-------------------------------------------------------------------------------
def ox_str_should_contain(str1, str2, ignore_case=True):
#-------------------------------------------------------------------------------
    """
    Give str1 and str2, return True if str1 contains str2

    Example input: 'abcccccdd'  'bCc'
    Example output:
            if ignore_case = True,   return True;
            if ignore_case = False,  return False;
    """

    # if ignore_case from input is string or int, convert to bool
    ignore_case = ox_get_bool(ignore_case)

    if ignore_case:
        return str2.lower() in str1.lower()
    else:
        return str2 in str1
